import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {
  searchForm = this.fb.group({
    username: ['', Validators.required],
  });

  @Output() search = new EventEmitter<string>();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  onSearch(searchPhrase: string) {
    // this.search.emit(this.searchForm.get('username')!.value);
    this.search.emit(searchPhrase);
  }
}
