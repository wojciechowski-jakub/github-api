import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable, tap } from 'rxjs';
import { GithubRepositoryAPI } from '../data/github-repository-api.model';
import { GithubRepository } from '../data/github-repository.model';
import { GithubRepositoriesService } from '../services/github-repositories.service';
import { FetchRepos } from '../store/actions/search.actions';
import { ReposState, ReposStateModel } from '../store/states/repos.state';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @Select(ReposState.repos) repositories$!: Observable<GithubRepository[]>;

  constructor(private store: Store) {}

  ngOnInit(): void {}

  search(searchPhrase: string) {
    this.store.dispatch(new FetchRepos(searchPhrase));
  }
}
