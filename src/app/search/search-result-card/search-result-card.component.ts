import { Component, Input, OnInit } from '@angular/core';
import { GithubRepository } from 'src/app/data/github-repository.model';

@Component({
  selector: 'app-search-result-card',
  templateUrl: './search-result-card.component.html',
  styleUrls: ['./search-result-card.component.scss'],
})
export class SearchResultCardComponent implements OnInit {
  @Input() repository: GithubRepository | null = null;

  constructor() {}

  ngOnInit(): void {}
}
