export interface GithubRepository {
  name: string;
  owner: string;
  branches: {
    name: string;
    lastCommitSHA: string;
  }[];
}
