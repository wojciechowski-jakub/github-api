export interface GithubRepositoryAPI {
  name: string;
  owner: {
    login: string;
  };
}
