export interface GithubBranchAPI {
  name: string;
  commit: {
    sha: string;
  };
}
