import { GithubRepository } from './../../data/github-repository.model';
import { GithubBranchAPI } from './../../data/github-branch-api.model';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { concatMap, last, map, mergeMap, tap, of, forkJoin } from 'rxjs';
import { GithubRepositoryAPI } from 'src/app/data/github-repository-api.model';
import { GithubRepositoriesService } from 'src/app/services/github-repositories.service';
import { FetchRepos } from '../actions/search.actions';

export interface ReposStateModel {
  repositories: GithubRepository[];
}

@State<ReposStateModel>({
  name: 'repos',
  defaults: {
    repositories: [],
  },
})
@Injectable()
export class ReposState {
  constructor(private repositoriesService: GithubRepositoriesService) {}

  @Selector()
  static repos(state: ReposStateModel) {
    return state.repositories;
  }

  @Action(FetchRepos)
  fetchRepos(ctx: StateContext<ReposStateModel>, action: FetchRepos) {
    return this.repositoriesService.getUserRepositories(action.phrase).pipe(
      concatMap((repositories) => {
        const branches = repositories.map((r) =>
          this.repositoriesService.getRepositoryBranches(r.owner.login, r.name)
        );
        return forkJoin([of(repositories), ...branches]);
      }),
      map(([repositories, ...branches]) => {
        const completeRepositories = repositories.map((repo, i) => {
          return {
            name: repo.name,
            owner: repo.owner.login,
            branches: this.mapBranches(branches[i]),
          };
        });
        return completeRepositories;
      }),
      tap((repositories: GithubRepository[]) => ctx.setState({ repositories }))
    );
  }

  private mapBranches(branches: GithubBranchAPI[]) {
    const repoBranches: { name: string; lastCommitSHA: string }[] = [];

    branches.forEach((branch) => {
      repoBranches.push({
        name: branch.name,
        lastCommitSHA: branch.commit.sha,
      });
    });

    return repoBranches;
  }
}
