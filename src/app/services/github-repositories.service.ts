import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { GithubBranchAPI } from '../data/github-branch-api.model';
import { GithubRepositoryAPI } from '../data/github-repository-api.model';
import { GithubRepository } from '../data/github-repository.model';

@Injectable({
  providedIn: 'root',
})
export class GithubRepositoriesService {
  API_URL = 'https://api.github.com';

  constructor(private http: HttpClient) {}

  getUserRepositories(username: string): Observable<GithubRepositoryAPI[]> {
    return this.http.get<GithubRepositoryAPI[]>(
      `${this.API_URL}/users/${username}/repos`
    );
  }

  getRepositoryBranches(
    owner: string,
    repository: string
  ): Observable<GithubBranchAPI[]> {
    return this.http.get<GithubBranchAPI[]>(
      `${this.API_URL}/repos/${owner}/${repository}/branches`
    );
  }
}
