import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeadersInterceptor } from './headers.interceptor';

export const interceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    multi: true,
    useClass: HeadersInterceptor,
  },
];
